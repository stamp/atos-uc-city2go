# DOCKER & ANSIBLE CityGo descriptors

The purpose of this folder is to provide the Docker descriptors for the task of amplifying STAMP configurations.


## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system setting 

## Prerequisites
```
Docker
Docker compose
Linux SO
Ansible
Linux SO
```

## Installing
```
git clone https://gitlab.atosresearch.eu/ari/stamp_docker_citygoApp
```

## How Docker works?
```
root@osboxes:/ShowcaseServer# docker-compose up -d

```

## Output
```
root@osboxes:/home/osboxes/Desktop/Proyectos/STAMP/atos-uc-city2go/ShowcaseServer# docker-compose up -d

root@osboxes:/home/osboxes/Proyectos/STAMP/WP2_ATOS_USE_CASE/stamp_docker_citygoApp/ShowcaseServer# docker ps -a

CONTAINER ID        IMAGE                  COMMAND                  CREATED             STATUS                      PORTS                      NAMES

f6ffba6488a1        showcaseserver_web     "python /webapp/Sh..."   17 minutes ago      Up 12 minutes               0.0.0.0:80->80/tcp         my_web

025136bd43f1        fiware/orion           "/usr/bin/contextB..."   17 minutes ago      Up 17 minutes               0.0.0.0:1026->1026/tcp     showcaseserver_orion_1

46b6394ae5a3        showcaseserver_db      "/usr/lib/postgres..."   17 minutes ago      Up 17 minutes               0.0.0.0:5432->5432/tcp     my_postgres

74efc56db296        fiware/cygnus-common   "/cygnus-entrypoin..."   17 minutes ago      Up 17 minutes               5050/tcp, 8081/tcp         cygnus-common

c6eed8bf636f        mongo:3.2              "docker-entrypoint..."   17 minutes ago      Up 17 minutes               0.0.0.0:27017->27017/tcp   showcaseserver_mongo_1

70152314445e        fiware/cygnus-ngsi     "/cygnus-entrypoin..."   17 minutes ago      Up 17 minutes               5050/tcp, 8081/tcp         cygnus-ngsi

02b714d04285        6f2be7c01668           "/bin/sh -c 'yum -..."   47 minutes ago      Exited (1) 35 minutes ago                              festive_bohr


root@osboxes:/home/osboxes/Desktop/Proyectos/STAMP/atos-uc-city2go/ShowcaseServer# docker logs 235e68b80789

Performing system checks...



System check identified no issues (0 silenced).



March 22, 2018 - 10:55:11

Django version 1.10.2, using settings 'showcase_server.settings'

Starting development server at http://0.0.0.0:80/

Starting development server at http://0.0.0.0:80/

Quit the server with CONTROL-C.

[22/Mar/2018 10:55:39] "GET /dashboard/ HTTP/1.1" 200 6292

[22/Mar/2018 10:55:39] "GET /static/dashboard/stylesheets/styles.css HTTP/1.1" 200 170581

[22/Mar/2018 10:55:39] "GET /static/dashboard/stylesheets/override.css HTTP/1.1" 200 374

[22/Mar/2018 10:55:39] "GET /static/dashboard/img/citizens_video_thumb.png HTTP/1.1" 200 117528

[22/Mar/2018 10:55:39] "GET /static/dashboard/img/logos/logo-atos.png HTTP/1.1" 200 19962

[22/Mar/2018 10:55:39] "GET /static/dashboard/img/logos/logo-slogan.png HTTP/1.1" 200 5231

[22/Mar/2018 10:55:39] "GET /static/dashboard/img/bus_routes_thumb.png HTTP/1.1" 200 132271

[22/Mar/2018 10:55:39] "GET /static/dashboard/img/citizens_thumb.png HTTP/1.1" 200 113913

[22/Mar/2018 10:55:40] "GET /static/dashboard/stylesheets/FI-WARE_style_guide/fonts/FI-WARE/desyrel/desyrel.woff HTTP/1.1" 200 34196

[22/Mar/2018 10:55:41] "GET /static/dashboard/img/logos/ico/favicon.ico HTTP/1.1" 200 1527
```


## How Ansible works?
Edit the ansible/hosts file and enter the IP address of the server you are deploying to

```
root@osboxes:/home/osboxes/Desktop/Proyectos/STAMP/Ansible# cd /etc/ansible/
root@osboxes:/etc/ansible# ls
ansible.cfg  hosts  playbook.yml  roles
root@osboxes:/etc/ansible# cat hosts 

# This is the default ansible 'hosts' file.
.....
.....

# Ex 2: A collection of hosts belonging to the 'webservers' group

[webservers]
## alpha.example.org
## beta.example.org
## 192.168.1.100
## 192.168.1.110
127.0.0.1
```

## Run ansible
```
root@osboxes:/stamp_citygo_ansible# ansible-playbook ansible.yml 
```

## Output

```
root@osboxes:/home/osboxes/Desktop/Proyectos/STAMP/stamp_citygo_ansible# ansible-playbook ansible.yml 

PLAY [Deploy an App using Ansible] ********************************************************************************************************************

TASK [Gathering Facts] ********************************************************************************************************************************
ok: [127.0.0.1]

TASK [createdb : Install PostgreSQL] ******************************************************************************************************************
ok: [127.0.0.1] => (item=[u'postgresql', u'postgresql-contrib', u'libpq-dev', u'python-psycopg2'])

TASK [createdb : Create user and gran access to database] *********************************************************************************************
ok: [127.0.0.1]

TASK [createdb : Start the PostgreSQL service] ********************************************************************************************************
ok: [127.0.0.1]

TASK [createdb : Import data into the database (using psql to pull in data from /..)] *****************************************************************
changed: [127.0.0.1]

TASK [createdb : Ensure database is created] **********************************************************************************************************
ok: [127.0.0.1]

TASK [createdb : Ensure user has access to the database] **********************************************************************************************
ok: [127.0.0.1]

TASK [createdb : Ensure user does not have unnecessary privileges] ************************************************************************************
ok: [127.0.0.1]

TASK [createApache : install apache2] *****************************************************************************************************************
ok: [127.0.0.1]

TASK [createApache : enabled mod_rewrite] *************************************************************************************************************
ok: [127.0.0.1]

TASK [createApp : Install system packages] ************************************************************************************************************
ok: [127.0.0.1] => (item=[u'python-pip', u'python-dev', u'python-psycopg2', u'git'])

TASK [createApp : Create directory for app] ***********************************************************************************************************
ok: [127.0.0.1]

TASK [createApp : Clone/pull project repo] ************************************************************************************************************
Username for 'https://gitlab.atosresearch.eu': fernando.mendez
Password for 'https://fernando.mendez@gitlab.atosresearch.eu': 
ok: [127.0.0.1]

TASK [createApp : Install python requirements.txt] ****************************************************************************************************
ok: [127.0.0.1]

TASK [createApp : django migrate] *********************************************************************************************************************
ok: [127.0.0.1]

TASK [createApp : django collectstatic] ***************************************************************************************************************
ok: [127.0.0.1]

TASK [createApp : Make manage.py exectutable] *********************************************************************************************************
ok: [127.0.0.1]

TASK [createApp : django run app] *********************************************************************************************************************

```

## Versioning
For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 



## Authors
* **Fernando Méndez Requena** - fernando.mendez@atos.net
